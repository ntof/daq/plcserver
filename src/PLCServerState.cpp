/*
 * PLCServerState.cpp
 *
 *  Created on: Jun 21, 2016
 *      Author: mdonze
 */

#include "PLCServerState.h"

#include <sstream>

#include "PLC.h"

namespace ntof {
namespace plc {

PLCServerState::PLCServerState(const std::string &svcName) : state_(svcName)
{
    state_.addStateValue(0, "OK", true);
    state_.setValue(0);
    state_.setAutoRefresh(false);
}

PLCServerState::~PLCServerState() {}

/**
 * Sets error coming from PLC object
 * @param plc PLC object
 * @param error Error message
 */
void PLCServerState::setPLCError(const PLC &plc, int snap7Err)
{
    if (updateError(plc, snap7Err))
    {
        state_.refresh();
    }
}

/**
 * Sets/Updates a custom PLC error
 * @param plc PLC reference raising the error
 * @param item TS7 item for memory address resolution
 * @param errMsg Custom error message (if empty, error will be cleared)
 */
void PLCServerState::setPLCError(const PLC &plc,
                                 TS7DataItem *item,
                                 const std::string &errMsg)
{
    if (updateError(plc, errMsg, item))
    {
        state_.refresh();
    }
}

/**
 * Clears errors related to PLC
 * @param plc
 */
void PLCServerState::clearPLCErrors(const PLC &plc, TS7DataItem *item)
{
    if (updateError(plc, "", item))
    {
        state_.refresh();
    }
}

/**
 * Clears errors related to PLC
 * @param plc
 */
void PLCServerState::clearPLCErrors(const PLC &plc)
{
    if (updateError(plc, 0))
    {
        state_.refresh();
    }
}

bool PLCServerState::updateError(const PLC &plc, int snap7Err, TS7DataItem *item)
{
    int errCode = computeErrorCode(plc, item);
    bool ret = false;

    ActiveSet::iterator eIt = activeErrors_.find(errCode);
    if (snap7Err != 0)
    {
        if (eIt == activeErrors_.end())
        {
            // New
            std::ostringstream oss;
            oss << "PLC error on " << plc.getPLCHostName();
            if (item != NULL)
            {
                oss << " [DB" << item->DBNumber << '.' << item->Start << ']';
            }
            oss << " : " << CliErrorText(snap7Err);
            activeErrors_.insert(errCode);
            state_.addError(errCode, oss.str());
            ret = true;
        }
    }
    else
    {
        if (eIt != activeErrors_.end())
        {
            activeErrors_.erase(errCode);
            state_.removeError(errCode);
            ret = true;
        }
    }
    return ret;
}

/**
 * Update error with custom message
 * @param plc
 * @param errMsg
 * @param item
 * @return
 */
bool PLCServerState::updateError(const PLC &plc,
                                 const std::string &errMsg,
                                 TS7DataItem *item)
{
    int errCode = computeErrorCode(plc, item, false);
    bool ret = false;

    ActiveSet::iterator eIt = activeErrors_.find(errCode);
    if (!errMsg.empty())
    {
        // Error active
        if (eIt == activeErrors_.end())
        {
            // New
            activeErrors_.insert(errCode);
            state_.addError(errCode, errMsg);
            ret = true;
        }
    }
    else
    {
        // Inactive
        if (eIt != activeErrors_.end())
        {
            activeErrors_.erase(errCode);
            state_.removeError(errCode);
            ret = true;
        }
    }
    return ret;
}

/**
 * Computes the error code
 * The error code is the following:
 * XPPDDDAAAA Where :
 * X is MSB bit to 1 if the error is not related to Snap 7
 * PP is PLC index
 * DDD is the DB number
 * AAAA is the DB address
 * @param plc Reference to PLC object
 * @param dbNumber Number of the DB raising the error
 * @param addr Address of the memory area in the DB raising the error
 * @param fromSnap7 If true, error is automatically reset when S7 readout if
 * back to normal
 * @return
 */
int PLCServerState::computeErrorCode(const PLC &plc,
                                     TS7DataItem *item,
                                     bool fromSnap7)
{
    int index = plc.getPLCIndex();
    int errCode = (fromSnap7 ? 0 : 0x80000000);
    errCode += index * 10000000;
    if (item != NULL)
    {
        errCode += item->DBNumber * 10000;
        errCode += item->Start;
    }
    return errCode;
}

/**
 * Sets reading result for specified PLC
 * @param items
 * @param count
 */
void PLCServerState::setReadingResults(const PLC &plc,
                                       TS7DataItem *items,
                                       int count)
{
    bool willRefresh = false;
    for (int i = 0; i < count; ++i)
    {
        willRefresh |= updateError(plc, items[i].Result, &items[i]);
    }
    if (willRefresh)
    {
        state_.refresh();
    }
}

} /* namespace plc */
} /* namespace ntof */
