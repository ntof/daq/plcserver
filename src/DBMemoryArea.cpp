/*
 * DBMemoryArea.cpp
 *
 *  Created on: Jun 14, 2016
 *      Author: mdonze
 */
#include "DBMemoryArea.h"

#include <algorithm>

#include "DataBlock.h"

namespace ntof {
namespace plc {

/**
 * Construct a memory area for fast-raw transfers
 * @param parentDB Associated DB object
 */
DBMemoryArea::DBMemoryArea(DataBlock *parentDB) : db_(parentDB)
{
    item_.Area = S7AreaDB;
    item_.pdata = NULL;
    item_.DBNumber = parentDB->getNumber();
    item_.WordLen = S7WLByte; // We will use raw byte data here...
}

DBMemoryArea::~DBMemoryArea() {}

/**
 * Gets the area data item
 * @return where to read
 */
TS7DataItem DBMemoryArea::getAreaDataItem()
{
    if (buffer_.empty())
    {
        buffer_.resize(item_.Amount);
        item_.pdata = buffer_.data();
    }
    return item_;
}

/**
 * Gets the start address in the DB
 * @return The starting address of the data
 */
int DBMemoryArea::getStartAddress() const
{
    if (!registers_.empty())
    {
        RegVector::const_iterator first = registers_.begin();
        int srcDB, srcAddr;
        (*first)->getSourceAddress(srcDB, srcAddr);
        return srcAddr;
    }
    else
    {
        // No registers, should not happen
        return 0;
    }
}

/**
 * Gets the last address to read (including last data)
 * For example, if we have a memory with 4 int starting at 0. the result will
 * be: Start : 0, End: 8 (because it includes the last 2 bytes)
 * @return
 */
int DBMemoryArea::getEndAddress() const
{
    if (!registers_.empty())
    {
        RegVector::const_iterator last = registers_.end();
        --last;
        int srcDB, srcAddr;
        (*last)->getSourceAddress(srcDB, srcAddr);
        srcAddr += (*last)->getDataLength();
        return srcAddr;
    }
    else
    {
        // No registers, should not happen
        return 0;
    }
}

/**
 * Adds a new register to this
 * @param reg
 */
void DBMemoryArea::addRegister(RegisterPtr reg)
{
    buffer_.clear();
    registers_.push_back(reg);
    std::sort(registers_.begin(), registers_.end(), RegisterCompare());

    // Recompute start address and amount of data to read
    RegVector::iterator first = registers_.begin();
    RegVector::iterator end = registers_.end() - 1;
    item_.Amount = ((*end)->getDataLength() + (*end)->getSourceDBAddress()) -
        registers_.begin()->get()->getSourceDBAddress();
    item_.Start = (*first)->getSourceDBAddress();
}

/**
 * Called when the reading of the PLC is done
 */
void DBMemoryArea::readDone(std::vector<TS7DataItem> &items)
{
    // Here we have the buffer refreshed
    int result = 0;
    for (std::vector<TS7DataItem>::iterator it = items.begin();
         it != items.end(); ++it)
    {
        if ((*it).pdata == item_.pdata)
        {
            result = it->Result;
            break;
        }
    }

    for (RegVector::iterator it = registers_.begin(); it != registers_.end();
         ++it)
    {
        if (result == 0)
        {
            int addr = (*it)->getSourceDBAddress() - item_.Start;
            (*it)->setData(&buffer_.data()[addr]);
        }
        else
        {
            (*it)->setData(0);
        }
    }
}

} /* namespace plc */
} /* namespace ntof */
