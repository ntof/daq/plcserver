/*
 * Register.h
 *
 *  Created on: Mar 30, 2015
 *      Author: mdonze
 */

#ifndef REGISTER_H_
#define REGISTER_H_

#include <memory>
#include <vector>

#include <DIMParamList.h>
#include <pugixml.hpp>
#include <snap7.h>

namespace ntof {
namespace plc {
class PLC;

/**
 * Represents a PLC register
 */
class Register
{
public:
    // TODO: Support arrays!
    enum DATA_TYPE
    {
        BYTE = 0,
        WORD,
        INT,
        DWORD,
        DINT,
        REAL,
        ENUM
    };

    /**
     * Constructor
     * @param regNode Configuration node of this register
     * @param destList Associated parameter list
     */
    Register(pugi::xml_node &regNode,
             ntof::dim::DIMParamList &destList,
             const PLC &plc);
    virtual ~Register();

    /**
     * Gets the data length
     * @return
     */
    int getDataLength();

    /**
     * Sets the data associated with this
     * @param destList Parameter list destination
     * @param pData Pointer to memory area (already at good location)
     */
    void setData(byte *pData);

    /**
     * Gets register name
     * @return The register name
     */
    const std::string &getRegisterName() const;

    /**
     * Gets source memory address
     * @param dbNumber DB number or -1 if not applicable
     * @param startAddr Start address of the data in DB or -1 if not applicable
     */
    void getSourceAddress(int &dbNumber, int &startAddr) const;

    /**
     * Gets source memory address
     * @param dbNumber DB number or -1 if not applicable
     * @param startAddr Start address of the data in DB or -1 if not applicable
     */
    int getSourceDBAddress() const;

    /**
     * Gets source memory DB number
     * @param dbNumber DB number or -1 if not applicable
     * @param startAddr Start address of the data in DB or -1 if not applicable
     */
    int getSourceDBNumber() const;

    /**
     * Gets destination memory address
     * @param dbNumber DB number or -1 if not applicable
     * @param startAddr Start address of the data in DB or -1 if not applicable
     */
    void getDestinationAddress(int &dbNumber, int &startAddr) const;

    /**
     * Gets register data item
     * @return
     */
    TS7DataItem getRegisterS7Data(ntof::dim::DIMData &data);

private:
    const PLC &plc; //!<< Reference to PLC
    int srcDBNum;   //!<< Source data block number
    int srcDBAddr;  //!<< Source data block base address
    int destDBNum;  //!<< Destination data block number (if write of this
                    //!< register is allowed)
    int destDBAddr; //!<< Destination data block base address (if write of this
                    //!< register is allowed)
    DATA_TYPE dataType;       //!<< Data type
    std::string regName;      //!<< Register name
    std::vector<byte> buffer; //!<< Memory for Snap 7
    ntof::dim::DIMParamList &m_paramsList;
    ntof::dim::DIMData::Index m_dataIndex; //!<< Associated DIMData from DIM
                                           //!< parameter list
};

typedef std::shared_ptr<Register> RegisterPtr; //!<< Typedef for smart pointer

struct RegisterCompare
{
    bool operator()(const RegisterPtr &a, const RegisterPtr &b);
};

} /* namespace plc */
} /* namespace ntof */

#endif /* REGISTER_H_ */
