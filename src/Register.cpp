/*
 * Register.cpp
 *
 *  Created on: Mar 30, 2015
 *      Author: mdonze
 */

#include "Register.h"

#include <DIMException.h>
#include <NTOFException.h>
#include <s7.h>

#include "PLC.h"
#include "PLCServer.h"

using namespace ntof::dim;
using namespace ntof::utils;

namespace ntof {
namespace plc {

/**
 * Constructor
 * @param regNode Configuration node of this register
 * @param associatedList Associated parameter list
 */
Register::Register(pugi::xml_node &regNode,
                   DIMParamList &destList,
                   const PLC &plc) :
    plc(plc), m_paramsList(destList)
{
    std::string regType = regNode.attribute("type").as_string("NONE");
    srcDBNum = regNode.attribute("srcDBNumber").as_int(-1);
    srcDBAddr = regNode.attribute("srcAddress").as_int(-1);
    if ((srcDBNum == -1) || (srcDBAddr == -1))
    {
        srcDBNum = -1;
        srcDBAddr = -1;
    }

    destDBNum = regNode.attribute("destDBNumber").as_int(-1);
    destDBAddr = regNode.attribute("destAddress").as_int(-1);
    if ((destDBNum == -1) || (destDBAddr == -1))
    {
        destDBNum = -1;
        destDBAddr = -1;
    }

    regName = regNode.attribute("name").as_string("UNNAMED");
    std::string regUnit = regNode.attribute("unit").as_string("");

    m_dataIndex = destList.getParameterCount();
    if (regType == "BYTE")
    {
        dataType = BYTE;
        destList.addParameter(m_dataIndex, regName, regUnit, (int32_t) 0,
                              AddMode::CREATE, false);
    }
    else if (regType == "WORD")
    {
        dataType = WORD;
        destList.addParameter(m_dataIndex, regName, regUnit, (int32_t) 0,
                              AddMode::CREATE, false);
    }
    else if (regType == "INT")
    {
        dataType = INT;
        destList.addParameter(m_dataIndex, regName, regUnit, (int32_t) 0,
                              AddMode::CREATE, false);
    }
    else if (regType == "DWORD")
    {
        dataType = DWORD;
        destList.addParameter(m_dataIndex, regName, regUnit, (int32_t) 0,
                              AddMode::CREATE, false);
    }
    else if (regType == "DINT")
    {
        dataType = DINT;
        destList.addParameter(m_dataIndex, regName, regUnit, (int32_t) 0,
                              AddMode::CREATE, false);
    }
    else if (regType == "REAL")
    {
        dataType = REAL;
        destList.addParameter(m_dataIndex, regName, regUnit, (double) 0.0,
                              AddMode::CREATE, false);
    }
    else if (regType == "ENUM")
    {
        dataType = ENUM;
        ntof::dim::DIMEnum dataEnum;
        //            dataEnum.addItem(-1, "OUT OF RANGE");
        //            dataEnum.setValue(-1);
        for (pugi::xml_node enumNode = regNode.first_child(); enumNode;
             enumNode = enumNode.next_sibling())
        {
            dataEnum.addItem(enumNode.attribute("value").as_int(0),
                             enumNode.attribute("name").as_string("UNKNOWN"));
        }
        destList.addParameter(m_dataIndex, regName, regUnit, dataEnum,
                              AddMode::CREATE, false);
    }
    else
    {
        throw NTOFException("Unknown PLC register type!", __FILE__, __LINE__);
    }
}

Register::~Register() {}

/**
 * Gets the data length
 * @return
 */
int Register::getDataLength()
{
    switch (dataType)
    {
    case BYTE: return 1;
    case WORD:
    case INT:
    case ENUM: // Enum is like a int
        return 2;
    case DWORD:
    case DINT: return 4;
    case REAL: return 4;
    }
    return 0;
}

/**
 * Sets the data associated with this
 * @param destList Parameter list destination
 * @param pData Pointer to memory area (already at good location)
 */
void Register::setData(byte *pData)
{
    LockRef<DIMData> data(m_paramsList.lockParameterAt(m_dataIndex));
    if (pData != NULL)
    {
        data->setHidden(false);
        int32_t value = 0;
        switch (dataType)
        {
        case BYTE: value = S7_GetByteAt(pData, 0); break;
        case WORD: value = S7_GetWordAt(pData, 0); break;
        case INT: value = S7_GetIntAt(pData, 0); break;
        case DWORD: value = S7_GetDWordAt(pData, 0); break;
        case DINT: value = S7_GetDIntAt(pData, 0); break;
        case REAL: data->setValue((double) S7_GetRealAt(pData, 0)); return;
        case ENUM:
            value = S7_GetIntAt(pData, 0);
            TS7DataItem s7Item;
            s7Item.DBNumber = srcDBNum;
            s7Item.Start = srcDBAddr;
            try
            {
                data->getEnumValue().setValue(value);
                PLCServer::getState()->clearPLCErrors(plc, &s7Item);
            }
            catch (const ntof::dim::DIMException &e)
            {
                //                    data->getEnumValue().setValue(-1);
                data->setHidden(true);
                PLCServer::getState()->setPLCError(
                    plc, &s7Item, "Enumeration value out of range!");
            }
            return;
        default: break;
        }
        data->setValue(value);
    }
    else
    {
        data->setHidden(true);
    }
}

/**
 * Gets register name
 * @return The register name
 */
const std::string &Register::getRegisterName() const
{
    return regName;
}

/**
 * Gets source memory address
 * @param dbNumber DB number or -1 if not applicable
 * @param startAddr Start address of the data in DB or -1 if not applicable
 */
void Register::getSourceAddress(int &dbNumber, int &startAddr) const
{
    dbNumber = srcDBNum;
    startAddr = srcDBAddr;
}

/**
 * Gets destination memory address
 * @param dbNumber DB number or -1 if not applicable
 * @param startAddr Start address of the data in DB or -1 if not applicable
 */
void Register::getDestinationAddress(int &dbNumber, int &startAddr) const
{
    dbNumber = destDBNum;
    startAddr = destDBAddr;
}

/**
 * Gets register data item
 * @return
 */
TS7DataItem Register::getRegisterS7Data(ntof::dim::DIMData &data)
{
    TS7DataItem ret;
    ret.Amount = 1;
    ret.Area = S7AreaDB;
    ret.DBNumber = destDBNum;
    ret.Start = destDBAddr;

    if (buffer.empty())
    {
        buffer.resize(getDataLength());
    }
    ret.pdata = buffer.data();

    switch (dataType)
    {
    case BYTE:
        ret.WordLen = S7WLByte;
        S7_SetByteAt(buffer.data(), 0, (uint8_t) (data.getIntValue() & 0xFF));
        break;
    case WORD:
        ret.WordLen = S7WLWord;
        S7_SetWordAt(buffer.data(), 0, (uint16_t) (data.getIntValue() & 0xFFFF));
        break;
    case INT:
        ret.WordLen = S7WLInt;
        S7_SetIntAt(buffer.data(), 0, (int16_t) (data.getIntValue() & 0xFFFF));
        break;
    case ENUM: {
        ret.WordLen = S7WLInt;
        int value = data.getEnumValue().getValue();
        if (value != -1)
        {
            S7_SetIntAt(buffer.data(), 0, (int16_t) (value & 0xFFFF));
        }
        else
        {
            throw NTOFException("Item is reserved!", __FILE__, __LINE__);
        }
    }
    break;
    case DWORD:
        ret.WordLen = S7WLDWord;
        S7_SetDWordAt(buffer.data(), 0, (uint32_t) (data.getIntValue()));
        break;
    case DINT:
        ret.WordLen = S7WLDInt;
        S7_SetDIntAt(buffer.data(), 0, data.getIntValue());
        break;
    case REAL:
        ret.WordLen = S7WLReal;
        S7_SetRealAt(buffer.data(), 0, (float) data.getDoubleValue());
        break;
    }
    return ret;
}

/**
 * Gets source memory address
 * @param dbNumber DB number or -1 if not applicable
 * @param startAddr Start address of the data in DB or -1 if not applicable
 */
int Register::getSourceDBAddress() const
{
    return srcDBAddr;
}

/**
 * Gets source memory DB number
 * @param dbNumber DB number or -1 if not applicable
 * @param startAddr Start address of the data in DB or -1 if not applicable
 */
int Register::getSourceDBNumber() const
{
    return srcDBNum;
}

bool RegisterCompare::operator()(const RegisterPtr &a, const RegisterPtr &b)
{
    return a->getSourceDBAddress() < b->getSourceDBAddress();
}

} /* namespace plc */
} /* namespace ntof */
