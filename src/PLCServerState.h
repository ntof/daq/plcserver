/*
 * PLCServerState.h
 *
 *  Created on: Jun 21, 2016
 *      Author: mdonze
 */

#ifndef PLCSERVERSTATE_H_
#define PLCSERVERSTATE_H_

#include <map>
#include <memory>
#include <set>
#include <string>

#include <DIMState.h>
#include <snap7.h>

namespace ntof {
namespace plc {
class PLC;

/**
 * This class is used to monitor the PLCreader status
 * The error code is the following:
 * XPPPDDDAAAA Where :
 * X is 1 if the error is not related to Snap 7
 * PPP is PLC index
 * DDD is the DB number
 * AAAA is the DB address
 */
class PLCServerState
{
public:
    /**
     * Constructor
     * @param svcName name of the DIMState service
     */
    explicit PLCServerState(const std::string &svcName);
    virtual ~PLCServerState();

    /**
     * Sets error coming from PLC object (connect error, reading error)
     * @param plc PLC object
     * @param error Error message
     */
    void setPLCError(const PLC &plc, int snap7Err);

    /**
     * Sets/Updates a custom PLC error
     * @param plc PLC reference raising the error
     * @param item TS7 item for memory address resolution
     * @param errMsg Custom error message (if empty, error will be cleared)
     */
    void setPLCError(const PLC &plc,
                     TS7DataItem *item,
                     const std::string &errMsg);

    /**
     * Clears errors related to PLC
     * @param plc
     */
    void clearPLCErrors(const PLC &plc);

    /**
     * Clears errors custom
     * @param plc
     */
    void clearPLCErrors(const PLC &plc, TS7DataItem *item);

    /**
     * Sets reading result for specified PLC
     * @param items
     * @param count
     */
    void setReadingResults(const PLC &plc, TS7DataItem *items, int count);

private:
    /**
     * Manage refresh of error
     * @param plc Associated PLC
     * @return True if DIM needs to be refreshed
     */
    bool updateError(const PLC &plc, int snap7Err, TS7DataItem *item = NULL);

    /**
     * Manage refresh of error
     * @param plc Associated PLC
     * @return True if DIM needs to be refreshed
     */
    bool updateError(const PLC &plc,
                     const std::string &errMsg,
                     TS7DataItem *item = NULL);

    /**
     * Computes the error code
     * The error code is the following:
     * XPPPDDDAAAA Where :
     * X is 1 if the error is not related to Snap 7
     * PPP is PLC index
     * DDD is the DB number
     * AAAA is the DB address
     * @param plc Reference to PLC object
     * @param dbNumber Number of the DB raising the error
     * @param addr Address of the memory area in the DB raising the error
     * @param fromSnap7 If true, error is automatically reset when S7 readout if
     * back to normal
     * @return
     */
    int computeErrorCode(const PLC &plc,
                         TS7DataItem *item = NULL,
                         bool fromSnap7 = true);

    ntof::dim::DIMState state_; //!<< State object
    typedef std::set<int> ActiveSet;
    //    typedef std::map<int, ActiveSet> PLCMap;
    ActiveSet activeErrors_;
};

typedef std::shared_ptr<PLCServerState> PLCServerStatePtr;

} /* namespace plc */
} /* namespace ntof */

#endif /* PLCSERVERSTATE_H_ */
