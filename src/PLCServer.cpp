/*
 * PLCServer.cpp
 *
 *  Created on: Mar 30, 2015
 *      Author: mdonze
 */

#include "PLCServer.h"

#include <iostream>

#include <ConfigMisc.h>
#include <NTOFException.h>
#include <pugixml.hpp>

// 4 TEST
#include "DBMemoryArea.h"
#include "DataBlock.h"
#include "PLCServerState.h"

namespace ntof {
namespace plc {
// Static member initialization
PLCServerStatePtr PLCServer::state_;

PLCServer::PLCServer() {}

PLCServer::~PLCServer() {}

bool PLCServer::loadConfig(const std::string &cfgPath,
                           const std::string &configMisc)
{
    pugi::xml_document doc;
    pugi::xml_parse_result res = doc.load_file(cfgPath.c_str());
    if (res)
    {
        try
        {
            ntof::utils::ConfigMisc cfg(configMisc);
            std::string dimDNS = cfg.getDimDns();
            std::cout << "Setting DIM DNS NODE to " << dimDNS << std::endl;
            DimServer::setDnsNode(dimDNS.c_str());

            pugi::xml_node root = doc.child("server");
            std::string serverName =
                root.attribute("serverName").as_string("PLCServer");
            if (state_.get() == NULL)
            {
                std::string stateName = serverName + "/State";
                state_ = PLCServerStatePtr(new PLCServerState(stateName));
            }

            pugi::xml_node plcsNode = root.child("plcs");
            for (pugi::xml_node plcNode = plcsNode.first_child(); plcNode;
                 plcNode = plcNode.next_sibling())
            {
                plcs.push_back(PLCPtr(new PLC(plcNode)));
            }

            DimServer::start(serverName.c_str());

            return true;
        }
        catch (const ntof::NTOFException &e)
        {
            std::cerr
                << "FATAL : Unable to load configuration file! : " << e.what()
                << std::endl;
            return false;
        }
    }
    else
    {
        std::cerr << "FATAL : Unable to parse XML configuration file "
                  << cfgPath << std::endl;
        return false;
    }
}

/**
 * Returns the PLCServerState object pointer
 * for updating an error
 * @return
 */
PLCServerStatePtr PLCServer::getState()
{
    return state_;
}

} /* namespace plc */
} /* namespace ntof */
