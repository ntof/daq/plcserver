/*
 * PLC.cpp
 *
 *  Created on: Mar 27, 2015
 *      Author: mdonze
 */

#include "PLC.h"

#include <chrono>
#include <iostream>
#include <sstream>

#include <NTOFException.h>
#include <NTOFLogging.hpp>
#include <arpa/inet.h>
#include <netdb.h>
#include <s7.h>
#include <snap7.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>

#include "DataBlock.h"
#include "PLCServer.h"
#include "PLCServerState.h"
#include "PLCService.h"

// Some structs from snap7 (mess to include it here)
typedef struct
{
    byte P;       // Telegram ID, always 32
    byte PDUType; // Header type 1 or 7
    word AB_EX; // AB currently unknown, maybe it can be used for long numbers.
    word Sequence; // Message ID. This can be used to make sure a received answer
    word ParLen;   // Length of parameters which follow this header
    word DataLen;  // Length of data which follow the parameters
} TS7ReqHeader;

typedef struct
{
    byte ItemHead[3];
    byte TransportSize;
    word Length;
    word DBNumber;
    byte Area;
    byte Address[3];
} TReqFunReadItem;

namespace ntof {
namespace plc {

int PLC::nextIndex = 0;

/**
 * Constructor
 * @param plcNode Node for configuring this PLC
 */
PLC::PLC(pugi::xml_node &plcNode) :
    maxPDU(0),
    refreshPeriod(plcNode.attribute("period").as_int(1000)),
    plcName(plcNode.attribute("hostname").as_string("")),
    plcIndex(++nextIndex)
{
    std::cout << "Building PLC with hostname : " << plcName << std::endl;
    for (pugi::xml_node destNode = plcNode.first_child(); destNode;
         destNode = destNode.next_sibling())
    {
        svc_.push_back(PLCServicePtr(new PLCService(destNode, *this)));
    }
    th = std::thread(&PLC::readProc, this);
}

PLC::~PLC() {}

/**
 * Connect to PLC
 * @return
 */
bool PLC::doConnect()
{
    if (client.Connected())
    {
        return true;
    }
    std::string plcIP;
    resolveIP(plcName, plcIP);
    std::cout << "Connecting to PLC " << plcName << " (" << plcIP << ")";
    // Client not connected
    int ret = client.ConnectTo(plcIP.c_str(), 0, 2);
    PLCServer::getState()->setPLCError(*this, ret);
    if (ret != 0)
    {
        std::cout << "Error!" << std::endl;
        return false;
    }
    TS7CpInfo info;
    client.GetCpInfo(&info);
    maxPDU = info.MaxPduLengt;
    std::cout << " ok! (PDU lenght : " << maxPDU << ")" << std::endl;
    return true;
}

int PLC::readMultipleRegs(TS7DataItem *items, int count)
{
    std::lock_guard<std::mutex> lock(mut);
    if (count != 0)
    {
        int ret = client.ReadMultiVars(items, count);
        return ret;
    }
    else
    {
        return 0;
    }
}

/**
 * Perform PLC reading
 */
void PLC::doRead()
{
    int totSize = sizeof(TS7ReqHeader);

    int ret = 0;

    // Gets all items
    std::vector<TS7DataItem> items;
    for (DBMap::iterator it = dbs_.begin(); it != dbs_.end(); ++it)
    {
        (*it).second->getDataItems(items);
    }
    TS7DataItem *item = &items[0];
    int itemCount = 0;
    for (std::vector<TS7DataItem>::iterator it = items.begin();
         it != items.end(); ++it)
    {
        int nextSize = totSize + (2 * sizeof(TReqFunReadItem));
        if ((nextSize <= maxPDU) && (itemCount < 20))
        {
            ++itemCount;
            totSize = nextSize;
        }
        else
        {
            // Needs to read (PSU will become too big)
            ret = readMultipleRegs(item, itemCount);
            if (ret == 0)
            {
                item = &(*it);
                itemCount = 1;
                totSize = sizeof(TS7ReqHeader) + (2 * sizeof(TReqFunReadItem));
            }
            else
            {
                break;
            }
        }
    }
    // Needs to read rest
    if (ret == 0)
    {
        ret = readMultipleRegs(item, itemCount);
    }

    if (ret != 0)
    {
        PLCServer::getState()->setPLCError(*this, ret);
        for (std::vector<TS7DataItem>::iterator it = items.begin();
             it != items.end(); ++it)
        {
            (*it).Result = errCliItemNotAvailable;
        }
        client.Disconnect();
    }
    // Update DB and registers
    for (DBMap::iterator it = dbs_.begin(); it != dbs_.end(); ++it)
    {
        (*it).second->readDone(items);
    }

    // Refresh the DIM parameter lists
    for (ServiceVector::iterator it = svc_.begin(); it != svc_.end(); ++it)
    {
        (*it)->updateList();
    }
    // Sets reading result to clear/set PLC error
    PLCServer::getState()->setReadingResults(*this, &items[0], items.size());
}

/**
 * Procedure to read PLC (thread)
 */
void PLC::readProc()
{
    struct timespec delay;
    int sec = refreshPeriod / 1000;
    refreshPeriod = refreshPeriod - sec * 1000;
    // perform the addition
    delay.tv_nsec = refreshPeriod * 1000000;
    delay.tv_sec = delay.tv_nsec / 1000000000 + sec;

    while (1)
    {
        try
        {
            nanosleep(&delay, NULL);
            if (doConnect())
            {
                std::chrono::high_resolution_clock::time_point start(
                    std::chrono::high_resolution_clock::now());
                doRead();
                //                std::cout << "Time to read : " <<
                //                std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::high_resolution_clock::now()
                //                - start).count() << std::endl;
            }
        }
        catch (...)
        {
            std::cerr << "PLC read failed on " << plcName
                      << ". Unknown exception caught!" << std::endl;
        }
    }
}

void PLC::resolveIP(const std::string &hostname, std::string &ipString)
{
    char addr[INET6_ADDRSTRLEN];
    struct addrinfo *result;
    int ret = ::getaddrinfo(hostname.c_str(), nullptr, nullptr, &result);
    if (ret != 0)
    {
        std::cerr << "failed to resolve " << hostname << " : "
                  << gai_strerror(ret) << std::endl;
    }
    else if (result->ai_family == AF_INET)
    {
        if (::inet_ntop(result->ai_family,
                        &((struct sockaddr_in *) result->ai_addr)->sin_addr,
                        addr, INET6_ADDRSTRLEN) == nullptr)
            std::cerr << "failed to convert address for " << hostname
                      << std::endl;
        else
            ipString = addr;
    }
    else if (result->ai_family == AF_INET6)
    {
        if (::inet_ntop(result->ai_family,
                        &((struct sockaddr_in6 *) result->ai_addr)->sin6_addr,
                        addr, INET6_ADDRSTRLEN) == nullptr)
            std::cerr << "failed to convert address for " << hostname
                      << std::endl;
        else
            ipString = addr;
    }
}
/**
 * Gets or create pointer to datablock
 * @param dbNumber
 * @return
 */
DataBlockPtr PLC::getDatablock(int dbNumber)
{
    DBMap::iterator it = dbs_.find(dbNumber);
    if (it == dbs_.end())
    {
        DataBlockPtr db(new DataBlock(dbNumber, *this));
        std::pair<DBMap::iterator, bool> p = dbs_.insert(
            std::pair<int, DataBlockPtr>(dbNumber, db));
        return p.first->second;
    }
    else
    {
        return it->second;
    }
}

/**
 * Prints debug information
 */
void PLC::printDebug()
{
    std::cout << "PLC name : " << plcName << std::endl;
    for (DBMap::iterator it = dbs_.begin(); it != dbs_.end(); ++it)
    {
        std::cout << "\tDB number : " << (*it).second->getNumber() << std::endl;
        std::vector<TS7DataItem> items;
        (*it).second->getDataItems(items);
        for (std::vector<TS7DataItem>::iterator itIt = items.begin();
             itIt != items.end(); ++itIt)
        {
            std::cout << "\t\tData from : " << itIt->Start
                      << " count : " << itIt->Amount << std::endl;
        }
    }
}

/**
 * Called by the associated PLCService to write some data on the PLC
 * @param items
 * @param count
 */
void PLC::writeMultipleRegs(TS7DataItem *items, int count)
{
    std::lock_guard<std::mutex> lock(mut);
    int ret = client.WriteMultiVars(items, count);
    // Test multiple write execution
    if (ret == 0)
    {
        for (int i = 0; i < count; ++i)
        {
            if (items[i].Result != 0)
            {
                ret = items[i].Result;
                break;
            }
        }
    }

    if (ret != 0)
    {
        std::ostringstream oss;
        oss << "Unable to write to PLC " << plcName << " => "
            << CliErrorText(ret);
        throw NTOFException(oss.str().c_str(), __FILE__, __LINE__, -2);
    }
}

/**
 * Gets the PLC hostname
 * @return
 */
const std::string &PLC::getPLCHostName() const
{
    return plcName;
}

/**
 * Gets PLC index
 * @return
 */
int PLC::getPLCIndex() const
{
    return plcIndex;
}

} /* namespace plc */
} /* namespace ntof */
