/*
 * DBMemoryArea.h
 *
 *  Created on: Jun 14, 2016
 *      Author: mdonze
 */

#ifndef DBMEMORYAREA_H_
#define DBMEMORYAREA_H_

#include <memory>
#include <vector>

#include <snap7.h>

#include "Register.h"

namespace ntof {
namespace plc {
class DataBlock;

/**
 * Represents a memory area in PLC DB
 * This class is mainly used to hold registers and start/end of the memory
 */
class DBMemoryArea
{
public:
    /**
     * Construct a memory area for fast-raw transfers
     * @param parentDB Associated DB object
     */
    explicit DBMemoryArea(DataBlock *parentDB);

    virtual ~DBMemoryArea();

    /**
     * Gets the area data item
     * @return where to read
     */
    TS7DataItem getAreaDataItem();

    /**
     * Gets the start address in the DB
     * @return The starting address of the data
     */
    int getStartAddress() const;

    /**
     * Gets the last address to read (including last data)
     * For example, if we have a memory with 4 int starting at 0. the result
     * will be: Start : 0, End: 8 (because it includes the last 2 bytes)
     * @return
     */
    int getEndAddress() const;

    /**
     * Adds a new register to this
     * @param reg
     */
    void addRegister(RegisterPtr reg);

    /**
     * Called when the reading of the PLC is done
     */
    void readDone(std::vector<TS7DataItem> &items);

private:
    DataBlock *db_;    //!< Associated data-block
    TS7DataItem item_; //!< Raw S7 memory area
    typedef std::vector<RegisterPtr> RegVector;
    RegVector registers_;      //!< Vector of registers
    std::vector<byte> buffer_; //!< Temporary used to put data
};

typedef std::shared_ptr<DBMemoryArea> DBMemoryAreaPtr;

} /* namespace plc */
} /* namespace ntof */

#endif /* DBMEMORYAREA_H_ */
