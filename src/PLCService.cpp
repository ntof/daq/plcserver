/*
 * PLCService.cpp
 *
 *  Created on: Jun 16, 2016
 *      Author: mdonze
 */

#include "PLCService.h"

#include <NTOFException.h>

#include "PLC.h"

namespace ntof {
namespace plc {

PLCService::PLCService(pugi::xml_node &svcNode, PLC &plc) :
    params_(svcNode.attribute("name").as_string()), plc_(plc)
{
    params_.setHandler(this);
    for (pugi::xml_node regNode = svcNode.first_child(); regNode;
         regNode = regNode.next_sibling())
    {
        ntof::plc::RegisterPtr reg(
            new ntof::plc::Register(regNode, params_, plc_));
        regs_[reg->getRegisterName()] = reg;

        int dbNumber = reg->getSourceDBNumber();
        if (dbNumber > 0)
        {
            DataBlockPtr db = plc_.getDatablock(dbNumber);
            db->addRegister(reg);
        }
    }
    params_.updateList();
}

PLCService::~PLCService() {}

void PLCService::updateList()
{
    params_.updateList();
}

int PLCService::parameterChanged(std::vector<dim::DIMData> &settingsChanged,
                                 const dim::DIMParamList & /*list*/,
                                 int &errCode,
                                 std::string &errMsg)
{
    try
    {
        std::vector<TS7DataItem> regsChanged;
        for (std::vector<dim::DIMData>::iterator it = settingsChanged.begin();
             it != settingsChanged.end(); ++it)
        {
            std::map<std::string, RegisterPtr>::iterator regIt = regs_.find(
                it->getName());
            if (regIt != regs_.end())
            {
                int dbDest, addDest;
                regIt->second->getDestinationAddress(dbDest, addDest);
                if ((dbDest == -1) || (addDest == -1))
                {
                    errCode = -1;
                    errMsg = "Register is read-only!";
                    return -1;
                }
                regsChanged.push_back(regIt->second->getRegisterS7Data(*it));
            }
        }
        plc_.writeMultipleRegs(&regsChanged[0], regsChanged.size());
    }
    catch (const NTOFException &e)
    {
        errCode = e.getErrorCode();
        errMsg = e.getMessage();
        return -2;
    }
    /* let the PLC update registers */
    settingsChanged.clear();
    return 0;
}

} /* namespace plc */
} /* namespace ntof */
