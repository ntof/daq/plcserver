/*
 * PLCServer.h
 *
 *  Created on: Mar 30, 2015
 *      Author: mdonze
 */

#ifndef PLCSERVER_H_
#define PLCSERVER_H_

#include <string>
#include <vector>

#include "PLC.h"
#include "PLCServerState.h"

namespace ntof {
namespace plc {

class PLCServer
{
public:
    PLCServer();
    virtual ~PLCServer();
    bool loadConfig(const std::string &cfgPath, const std::string &configMisc);
    static PLCServerStatePtr getState();

private:
    std::vector<PLCPtr> plcs;
    static PLCServerStatePtr state_;
};

} /* namespace plc */
} /* namespace ntof */

#endif /* PLCSERVER_H_ */
