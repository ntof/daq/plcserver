/*
 * DataBlock.cpp
 *
 *  Created on: Jun 14, 2016
 *      Author: mdonze
 */

#include "DataBlock.h"

#include <algorithm>
#include <iostream>

// Allow to read unused MEM_GAP
#define MEM_GAP 8

namespace ntof {
namespace plc {

DataBlock::DataBlock(int number, PLC &plc) : dbNumber_(number), plc_(plc)
{
    std::cout << "Adding a new DB " << number << " to PLC" << std::endl;
}

DataBlock::~DataBlock() {}

/**
 * Gets the DB number of this
 * @return
 */
const int DataBlock::getNumber() const
{
    return dbNumber_;
}

/**
 * Adds a new register to this DB
 * @param reg
 */
void DataBlock::addRegister(RegisterPtr reg)
{
    regs_.push_back(reg);
}

/**
 * Compact into memory areas
 */
void DataBlock::compactMemory()
{
    memAreas.clear();
    std::sort(regs_.begin(), regs_.end(), RegisterCompare());

    for (std::vector<RegisterPtr>::iterator it = regs_.begin();
         it != regs_.end(); ++it)
    {
        RegisterPtr reg = *it;
        // Try to find an existing range
        int regStart = reg->getSourceDBAddress();
        int regEnd = reg->getDataLength() + regStart;
        DBMemoryAreaPtr ptr;
        for (MemVector::iterator areaIt = memAreas.begin();
             areaIt != memAreas.end(); ++areaIt)
        {
            int start = (*areaIt)->getStartAddress();
            int end = (*areaIt)->getEndAddress();
            if ((abs(start - regStart) < MEM_GAP) ||
                (abs(end - regStart) < MEM_GAP) ||
                ((regStart > start) && (regEnd < end)))
            {
                // In range, add the register to existing memory pointer
                ptr = (*areaIt);
                break;
            }
        }
        if (ptr.get() == NULL)
        {
            // No found in known DBMemoryArea collection, create a new one
            ptr.reset(new DBMemoryArea(this));
            memAreas.push_back(ptr);
        }
        // Adds the register to the DBMemoryArea
        ptr->addRegister(reg);
    }
}

/**
 * Gets all associated data items to be read
 * @param items
 */
void DataBlock::getDataItems(std::vector<TS7DataItem> &items)
{
    // Compact memory if not done
    if (memAreas.empty())
    {
        compactMemory();
    }
    // Adds all TS7DataItem associated with
    for (MemVector::iterator it = memAreas.begin(); it != memAreas.end(); ++it)
    {
        items.push_back((*it)->getAreaDataItem());
    }
}

/**
 * Gets associated PLC
 * @return
 */
PLC &DataBlock::getPLC()
{
    return plc_;
}

/**
 * Called-back by the PLC when the reading of the PLC is done
 */
void DataBlock::readDone(std::vector<TS7DataItem> &items)
{
    for (MemVector::iterator it = memAreas.begin(); it != memAreas.end(); ++it)
    {
        (*it)->readDone(items);
    }
}

} /* namespace plc */
} /* namespace ntof */
