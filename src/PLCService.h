/*
 * PLCService.h
 *
 *  Created on: Jun 16, 2016
 *      Author: mdonze
 */

#ifndef PLCSERVICE_H_
#define PLCSERVICE_H_

#include <memory>

#include <DIMParamList.h>
#include <pugixml.hpp>

#include "Register.h"

namespace ntof {
namespace plc {
class PLC;
/**
 * Represents one DIM parameter list for publishing data
 */
class PLCService : public dim::DIMParamListHandler
{
public:
    /**
     * Constructor
     * @param svcNode Service configuration node
     */
    PLCService(pugi::xml_node &svcNode, PLC &plc);
    virtual ~PLCService();

    void updateList();

protected:
    /**
     * Parameter list handler
     * @param settingsChanged
     * @param list
     * @param errCode
     * @param errMsg
     * @return
     */
    int parameterChanged(std::vector<dim::DIMData> &settingsChanged,
                         const dim::DIMParamList &list,
                         int &errCode,
                         std::string &errMsg) override;

    dim::DIMParamList params_; //!< DIM Parameters read/set from PLC
    PLC &plc_;                 //!< Associated PLC
    std::map<std::string, RegisterPtr> regs_; //!< List of known registers for
                                              //!< this service
};

typedef std::shared_ptr<PLCService> PLCServicePtr;

} /* namespace plc */
} /* namespace ntof */

#endif /* PLCSERVICE_H_ */
