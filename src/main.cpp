/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-09-03T14:36:40+02:00
**     Author: Sylvain Fargier <sfargier> <sylvain.fargier@cern.ch>
**
*/

#include <string>

#include "PLCServer.h"

#define CFG_PATH "/etc/ntof/PLCServer.xml"
#define CFG_MISC "/etc/ntof/misc.xml"

int main(int argc, char **argv)
{
    const std::string cfgName = (argc > 1) ? std::string(argv[1]) :
                                             std::string(CFG_PATH);
    const std::string cfgMisc = (argc > 2) ? std::string(argv[2]) :
                                             std::string(CFG_MISC);

    ntof::plc::PLCServer p;
    if (p.loadConfig(cfgName, cfgMisc))
    {
        while (1)
        {
            sleep(1);
        }
    }
    return 0;
}
