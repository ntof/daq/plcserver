/*
 * PLC.h
 *
 *  Created on: Mar 27, 2015
 *      Author: mdonze
 */

#ifndef PLC_H_
#define PLC_H_

#include <map>
#include <memory>
#include <string>
#include <thread>
#include <vector>

#include <DIMParamList.h>
#include <pugixml.hpp>

#include "DataBlock.h"
#include "PLCService.h"
#include "Register.h"

namespace ntof {
namespace plc {

/**
 * This class represents a PLC
 */
class PLC
{
public:
    /**
     * Constructor
     * @param plcNode Node for configuring this PLC
     */
    explicit PLC(pugi::xml_node &plcNode);
    virtual ~PLC();

    /**
     * Gets or create pointer to datablock
     * @param dbNumber
     * @return
     */
    DataBlockPtr getDatablock(int dbNumber);

    /**
     * Prints debug information
     */
    void printDebug();

    void writeMultipleRegs(TS7DataItem *items, int count);

    /**
     * Gets the PLC hostname
     * @return
     */
    const std::string &getPLCHostName() const;

    /**
     * Gets PLC index
     * @return
     */
    int getPLCIndex() const;

private:
    static int nextIndex;

    /**
     * Convert hostname to IP
     * @param hostname
     * @param ipString
     */
    static void resolveIP(const std::string &hostname, std::string &ipString);

    /**
     * Procedure to read PLC (thread)
     */
    void readProc();

    /**
     * Connect to PLC
     * @return
     */
    bool doConnect();

    /**
     * Perform PLC reading
     */
    void doRead();

    int readMultipleRegs(TS7DataItem *items, int count);

    typedef std::map<int, DataBlockPtr> DBMap;
    typedef std::vector<PLCServicePtr> ServiceVector;
    int maxPDU;
    int refreshPeriod;   //!< Refresh period of acquisition
    std::string plcName; //!< PLC IP address in string format
    int plcIndex;        //!< Index of the PLC
    std::thread th;      //!< Reader thread
    TS7Client client;    //!< PLC client
    DBMap dbs_;          //!< Map of DB used on this PLC
    ServiceVector svc_;  //!< Vector of DIM services associated with this PLC
    std::mutex mut;      //!< Lock
};

typedef std::shared_ptr<PLC> PLCPtr;

} /* namespace plc */
} /* namespace ntof */

#endif /* PLC_H_ */
