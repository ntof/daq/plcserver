/*
 * DataBlock.h
 *
 *  Created on: Jun 14, 2016
 *      Author: mdonze
 */

#ifndef DATABLOCK_H_
#define DATABLOCK_H_

#include <memory>

#include "DBMemoryArea.h"
#include "Register.h"

namespace ntof {
namespace plc {
class PLC;
class DataBlock
{
public:
    /**
     * Constructor
     * @param number Number of DB to create
     */
    DataBlock(int number, PLC &plc);
    virtual ~DataBlock();

    /**
     * Gets the DB number of this
     * @return
     */
    const int getNumber() const;

    /**
     * Adds a new register to this DB
     * @param reg
     */
    void addRegister(RegisterPtr reg);

    /**
     * Gets associated PLC
     * @return
     */
    PLC &getPLC();

    /**
     * Compact into memory areas
     */
    void compactMemory();

    /**
     * Gets all associated data items to be read
     * @param items
     */
    void getDataItems(std::vector<TS7DataItem> &items);

    /**
     * Called when the reading of the PLC is done
     */
    void readDone(std::vector<TS7DataItem> &items);

private:
    int dbNumber_; // Datablock number
    typedef std::vector<DBMemoryAreaPtr> MemVector;
    MemVector memAreas;
    PLC &plc_;
    std::vector<RegisterPtr> regs_;
};

typedef std::shared_ptr<DataBlock> DataBlockPtr;

} /* namespace plc */
} /* namespace ntof */

#endif /* DATABLOCK_H_ */
