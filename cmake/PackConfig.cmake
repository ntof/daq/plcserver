
if (EXISTS "${CMAKE_CURRENT_LIST_DIR}/../.git/shallow")
  message(SEND_ERROR "Not generating package on shallow clone, use \"git fetch --unshallow\"")
endif ()
