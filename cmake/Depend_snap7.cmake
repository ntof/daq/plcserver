
include(Tools)

find_path(SNAP7_INCLUDE_DIRS NAMES s7.h PATH_SUFFIXES snap7)
find_library(SNAP7_LIBRARIES NAMES snap7)

assert(SNAP7_INCLUDE_DIRS AND SNAP7_LIBRARIES MESSAGE "Failed to find snap7")
